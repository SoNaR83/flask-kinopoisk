# flask-kinopoisk

This project is designed to adapt http requests for vCatalogizator via backend on Python

# Used technologies
- [flask](https://flask.palletsprojects.com/en/2.1.x/) as backend
- [NGINX](https://nginx.org/ru/) as request proxy-server

 ```mermaid
flowchart LR 
    A(fa:fa-user User) --> B[fa:fa-LabVIEW LabVIEW application]
    B --> C[fa:fa-flask Backend]
    C --> D(fa:fa-API Kinopoisk Request API)
```

# Backend

## nginx server
NGINX is used to proxy all http requests from 5001 port of remote domain to 5000 port that uses backend on flask microframework

## flask microframework
Backend uses a flask microframework. In our cases is need to process url get requests from LV application and redirect it to 3d party kinopoisk API

## patterns
This project uses five patterns (endpoints):  
    - find all movies matching criteria from  {movie title string}  
    - find movie description by movie ID on https://kinopoisk.ru  
    - find stuff description by movie ID on https://kinopoisk.ru  
    - find poster of movie ID on https://kinopoisk.ru  
    - find JSON-key data from movie ID

## endpoints:
request:
```
GET api/movie/id/<int:id_kinopoisk> 
this request returns all information about movie that matches to id_kinopoisk

GET api/stuff/id/<int:id_kinopoisk> 
this request returns all stuff from movie that matches to id_kinopoisk

GET api/poster/id/<int:id_kinopoisk> 
this request returns url to poster from movie that matches to id_kinopoisk

GET /api/key/<int:id_kinopoisk>/<string:key>
this request finds json key from movie description that matches to id_kinopoisk 

GET /api/find/<string:text>
this request returns any movie whose text matches the text entered by the user 
```

# Frontend
LabVIEW application uses as frontend. This app consists of any VIs from LabVIEW 2020 HTTP Client palette
