import json
import unittest

from ..src.Classes.kinopoiskCore import MovieFind


class TestKinopoisk(unittest.TestCase):
    # setUp method is overriden from the parent class TestCase
    def setUp(self):
        self.kino = MovieFind('02cffebd-d21d-4db4-9008-f21894dd5f21')

    def test_find_by_kinopoisk_id(self):
        movie_data = self.kino.find_by_id('666')
        self.assertEqual(json.loads(movie_data)['nameRu'], 'Форсаж')

    def test_find_stuff(self):
        stuff = self.kino.find_stuff_by_movie_id(666)
        if len(stuff) > 0:
            self.assertEqual(len(stuff), len(stuff))
        else:
            self.assertEqual(len(stuff), 0)

    def test_find_poster(self):
        poster = self.kino.get_poster(666)
        self.assertEqual(poster,
                         'https://avatars.mds.yandex.net/get-kinopoisk-image/6201401/8277905e-aa09-465d-b0de-7c389a42f215/x1000')

    def test_find_by_key(self):
        self.assertEqual(self.kino.find_by_key('666', 'nameRu'), 'Форсаж')

    def test_find_by_text(self):
        data_movie = json.loads(self.kino.find_by_text('Белые цыпочки')[1])['films']
        if len(data_movie) > 0:
            self.assertEqual(len(data_movie), len(data_movie))
        else:
            self.assertEqual(0, -1)

    def test_list_keys(self):
        keys = self.kino.list_keys(666)
        if type(keys) == list:
            self.assertEqual(type(keys), list)
        else:
            self.assertEqual(type(keys), type(keys))


if __name__ == '__main__':
    unittest.main()

# kino = MovieFind('02cffebd-d21d-4db4-9008-f21894dd5f21')
# keys_list = kino.list_keys(666)
# print(type(keys_list))
