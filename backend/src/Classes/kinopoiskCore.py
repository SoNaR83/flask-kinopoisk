import requests
import json


class MovieFind:
    def __init__(self, token=''):
        self.token = token
        self.headers = {'Content-Type': 'application/json', 'X-API-KEY': self.token}

    def find_by_id(self, film_id):
        url_movie = f'https://kinopoiskapiunofficial.tech/api/v2.2/films/{str(film_id)}'
        """Выполняем запросы"""
        # выполняем запрос на "Кино-поиск"
        movie_response = requests.get(url_movie, headers=self.headers)
        # Обрабатываем полученные запросы
        # Если коды запросов movie_response и stuff_response равны 200, то можно обработать запрос с постером
        if movie_response.status_code == 200:
            return movie_response.text
        elif movie_response.status_code == 401:
            return f"{movie_response.status_code} - Wrong Token"
        elif movie_response.status_code == 402:
            return f"{movie_response.status_code} - You exceeded the quota. You have sent 503 request, " \
                   f"but available 500 per day"
        elif movie_response.status_code == 404:
            return f"{movie_response.status_code} - Movie not found"
        elif movie_response.status_code == 429:
            return f"{movie_response.status_code} - Too many request rate. Request rate shall not" \
                   f"exceed at 20 request/sec)"
        else:
            return f"Can\'t process non-standard case: " \
                   f"{movie_response.status_code} "

    def find_stuff_by_movie_id(self, film_id):

        url_stuff = f'https://kinopoiskapiunofficial.tech/api/v1/staff?filmId={str(film_id)}'
        """Выполняем запросы"""
        # выполняем запрос на "Кино-поиск"
        stuff_response = requests.get(url_stuff, headers=self.headers)
        if stuff_response.status_code == 200:
            # # добавляем данные об актерах
            return stuff_response.text
        elif stuff_response.status_code == 401:
            return f"{stuff_response.status_code} - Wrong Token"
        elif stuff_response.status_code == 402:
            return f"{stuff_response.status_code} - You exceeded the quota. You have sent 503 request, " \
                   f"but available 500 per day"
        elif stuff_response.status_code == 404:
            return f"{stuff_response.status_code} - Movie not found"
        elif stuff_response.status_code == 429:
            return f"{stuff_response.status_code} - Too many request rate. Request rate shall not" \
                   f"exceed at 20 request/sec"
        else:
            return f"Can\'t process non-standard case: " \
                   f"{stuff_response.status_code} "

    def find_by_text(self, text):
        resp_film = []
        url = f'https://kinopoiskapiunofficial.tech/api/v2.1/films/search-by-keyword?keyword={text}'
        try:
            r = requests.get(url, headers=self.headers)
            resp_film.append(str(r.status_code))
            resp_film.append(r.text)
            return resp_film
        except requests.exceptions.RequestException:
            return resp_film

    def find_by_key(self, film_id, key):
        url_movie = f'https://kinopoiskapiunofficial.tech/api/v2.2/films/{str(film_id)}'
        try:
            movie_response = requests.get(url_movie, headers=self.headers)  # выполняем запрос на Кино-поиск
            try:
                return json.loads(movie_response.text)[key]
            except KeyError:
                return f'Key \'{key}\' not found'
        except requests.exceptions.RequestException:  # This is the correct syntax
            return 'Can\'t connect to server'

    def get_poster(self, film_id):
        url_movie = f'https://kinopoiskapiunofficial.tech/api/v2.2/films/{str(film_id)}'
        # выполняем запрос на "Кино-поиск"
        movie_response = requests.get(url_movie, headers=self.headers)
        if movie_response.status_code == 200:
            return (requests.get(json.loads(movie_response.text)['posterUrl'])).url
        elif movie_response.status_code == 401:
            return f"{movie_response.status_code} - Wrong Token"
        elif movie_response.status_code == 402:
            return f"{movie_response.status_code} - You exceeded the quota. You have sent 503 request, " \
                   f"but available 500 per day"
        elif movie_response.status_code == 404:
            return f"{movie_response.status_code} - Movie not found"
        elif movie_response.status_code == 429:
            return f"{movie_response.status_code} - Too many request rate. Request rate shall not" \
                   f"exceed at 20 request/sec"
        else:
            return f"Can\'t process non-standard case: " \
                   f"{movie_response.status_code} "

    def list_keys(self, film_id):
        url_movie = f'https://kinopoiskapiunofficial.tech/api/v2.2/films/{str(film_id)}'
        """Выполняем запросы"""
        # выполняем запрос на "Кино-поиск"
        movie_response = requests.get(url_movie, headers=self.headers)
        keys_list = [key for key in json.loads(movie_response.text)]
        return keys_list


if __name__ == '__main__':
    pass
