import json

from flask import Flask, send_file
from markupsafe import escape
from Classes.kinopoiskCore import MovieFind
import os

token_kinopoisk = os.environ.get('TOKEN')
movie = MovieFind(token_kinopoisk)

app = Flask(__name__)


@app.route("/")
def index():
    message = '<body bgcolor="#322f32"><h2 style=\'color:#ab14a3\'>Hi! This is my first flask app server!</h2><br>' \
              '<font size="+2"><font color="#09fbc6">This server was designed for interact to \'' \
              'https://kinopoiskapiunofficial.tech/\'<br>' \
              'For request to https://kinopoiskapiunofficial.tech/profile ' \
              'use following patterns:<br>' \
              '<b> - /api/movie/id/"int:id"</b> <font color=#8bebf0>to find movie by kinopoisk_id</font><br>' \
              '<b> - /api/stuff/id/"int:id"</b> ' \
              '<font color=#8bebf0>to find all staff for selected movie by kinopoisk_id</font><br>' \
              '<b> - /api/poster/id/"int:id"</b>' \
              '<font color=#8bebf0> to find url poster movie by kinopoisk_id</font><br>' \
              '<b> - /api/key/movie_id/"string:text"</b> ' \
              '<font color=#8bebf0>to find any information from json by requested key</font><br>' \
              '<b> - /api/find/"string:text"</b>' \
              '<font color=#8bebf0>to find any movie by keyword</font><br>' \
              '<b> - /api/keys/"int:id"</b>' \
              '<font color=#8bebf0>to find all available JSON keys </font><br>' \
              '<br>' \
              '<b> - /api/test/movie/{int}</b>' \
              '<font color=#8bebf0>test response movie json </font><br>' \
              '<b> - /api/test/movie/image/{int}</b>' \
              '<font color=#8bebf0>test response movie poster </font>'

    return message


@app.route("/api/movie/id/<int:id_kinopoisk>")
def find_by_kinopoisk_id(id_kinopoisk: int) -> str:
    return movie.find_by_id(id_kinopoisk)


@app.route("/api/stuff/id/<int:id_kinopoisk>")
def find_stuff(id_kinopoisk: int) -> str:
    return movie.find_stuff_by_movie_id(id_kinopoisk)


@app.route("/api/poster/id/<int:id_kinopoisk>")
def find_poster(id_kinopoisk: int) -> bytes:
    return movie.get_poster(id_kinopoisk)


@app.route("/api/key/<int:id_kinopoisk>/<key>")
def find_by_key(id_kinopoisk, key: int) -> dict:
    return movie.find_by_key((escape(id_kinopoisk)), escape(key))


@app.route("/api/find/<text>")
def find_by_text(text: str) -> str:
    return str(movie.find_by_text(text))


@app.route("/api/keys/<int:id>")
def list_keys(key: str) -> str:
    return str(movie.list_keys(escape(key)))


@app.route("/api/test/movie/<int:key>")
def ret_data(key: int) -> str:
    json_path = ''
    for file in os.listdir(f"data/{key}"):
        if '.json' in file:
            json_path = os.getcwd() + os.path.sep + 'data' + os.path.sep + f"{key}" + os.path.sep + file
            break

    with open(json_path, encoding='utf-8') as file:
        data = json.load(file)

    return json.dumps(data, ensure_ascii=False)


@app.route("/api/test/movie/image/<int:key>")
def get_test(key):
    return send_file(f'./data/{key}/image.jpg')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
