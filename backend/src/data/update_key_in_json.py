import glob
import json
import os


def update_key(key: str, new_wal: str) -> bool:
    pass


# for root, subdirs, files in os.walk("data"):
#     print('--\nroot = ' + root)
#     list_file_path = os.path.join(root, 'my-directory-list.txt')
#     print('list_file_path = ' + list_file_path)

img_base_url = "https://vds2284398.my-ihor.ru/kino-flask/api/test/movie/image/"

json_files = []
for count, filename in enumerate(glob.iglob('' + '**/*.json', recursive=True)):
    print(filename)
    print(os.listdir()[count])
    with open(filename, encoding='utf-8') as json_file:
        data = json.load(json_file)
    data["posterUrl"] = img_base_url+str(data["kinopoiskId"])
    with open(filename, "w",encoding='utf-8') as saved_json:
        json.dump(data, saved_json, ensure_ascii=False)



# for filename in glob.iglob('' + '**/*.jpg', recursive=True):
#     with open(filename, encoding='utf-8') as json_file:
#         data = json.load(json_file)
#         print(data)

